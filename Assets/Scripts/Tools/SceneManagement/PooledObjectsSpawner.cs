using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.Tools
{
    public class PooledObjectsSpawner : MonoBehaviour
    {

        private Dictionary<PoolableMonoBehavior, List<PoolableMonoBehavior>> _pooledObjects;

        private static PooledObjectsSpawner _instance;
        public static PooledObjectsSpawner Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<PooledObjectsSpawner>();
                    if (_instance == null)
                    {
                        _instance = new GameObject("ObjectPooler").AddComponent<PooledObjectsSpawner>();
                    }
                }
                return _instance;
            }
        }

        public T Spawn<T>(T prefab, Vector3 pos, Quaternion rot, Transform parent) where T : PoolableMonoBehavior
        {
            if (_pooledObjects == null)
            {
                _pooledObjects = new Dictionary<PoolableMonoBehavior, List<PoolableMonoBehavior>>();
            }

            if (_pooledObjects.ContainsKey(prefab))
            {
                for (var i = 0; i < _pooledObjects[prefab].Count; i++)
                {
                    var poolable = _pooledObjects[prefab][i];
                    if (!poolable.IsEnabled)
                    {
                        poolable.ResetGO();
                        poolable.transform.position = pos;
                        poolable.transform.rotation = rot;
                        poolable.transform.parent = parent;
                        return poolable as T;
                    }
                }

                var result = Instantiate(prefab, pos, rot, parent);
                _pooledObjects[prefab].Add(result);
                return result as T;
            } else
            {
                _pooledObjects[prefab] = new List<PoolableMonoBehavior>();
                _pooledObjects[prefab].Add(Instantiate(prefab, pos, rot, parent));
                return _pooledObjects[prefab][0] as T;
            }
        }
    }
}