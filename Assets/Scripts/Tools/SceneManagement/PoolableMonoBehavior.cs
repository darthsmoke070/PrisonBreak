using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.Tools
{

    public abstract class PoolableMonoBehavior : MonoBehaviour
    {

        public virtual bool IsEnabled => gameObject.activeSelf;

        public virtual void Enable(bool immediately)
        {
            gameObject.SetActive(true);
        }

        public virtual void Disable(bool immediately)
        {
            gameObject.SetActive(false);
        }

        public virtual void ResetGO()
        {

        }
    }
}
