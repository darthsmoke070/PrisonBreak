namespace PrisonBreak.Tools
{
    public static class Math
    {
        public static float Normalize360Angle(float angle)
        {
            return angle % 360f;
        }

        public static float Normalize180Angle(float angle)
        {
            angle = Normalize360Angle(angle);
            if (angle > 180f)
            {
                angle -= 360f;
            }
            return angle;
        }
    }
}
