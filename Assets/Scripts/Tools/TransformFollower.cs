using UnityEngine;

namespace PrisonBreak.Tools
{
    public class TransformFollower : MonoBehaviour
    {
        [SerializeField]
        private Transform _followTo;

        [SerializeField]
        private bool _followRotation;

        private void Update()
        {
            transform.position = _followTo.position;
            if (_followRotation)
            {
                transform.rotation = _followTo.rotation;
            }
        }

        public static TransformFollower Generate(Transform followTo, Quaternion initialRot)
        {
            var go = new GameObject($"{followTo} follower");
            var result = go.AddComponent<TransformFollower>();
            result._followTo = followTo;
            result.transform.rotation = initialRot;
            return result;
        }
    }
}
