using UnityEngine.UI;
using UnityEngine;

namespace PrisonBreak.Tools
{
    public class CustomCanvasScaler : CanvasScaler
    {
        private bool _isWideScree;

        protected override void Handle()
        {
            _isWideScree = 1f * Screen.width / Screen.height > referenceResolution.x / referenceResolution.y;
            matchWidthOrHeight = _isWideScree ? 1f : 0f;
            base.Handle();
        }

        public Vector2 GetScreenToCanvasPos(Vector2 point)
        {
            var coef = _isWideScree ? referenceResolution.y / Screen.height : 
                                      referenceResolution.x / Screen.width;
            return point * coef;
        }
    }
}
