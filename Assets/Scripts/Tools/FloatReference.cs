using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.Tools
{
    [CreateAssetMenu]
    public class FloatReference : ScriptableObject
    {
        public float Value;    
    }
}
