using PrisonBreak.Tools;
using UnityEngine;
using UnityEngine.UI;

namespace PrisonBreak.UI
{
    public class LoadScreen : Screen
    {
        [SerializeField]
        private FillBar _progressBar;

        [SerializeField]
        private FloatReference _loadingProgress;

        public void Init(FloatReference loadingProgress)
        {
            _loadingProgress = loadingProgress;
        }

        private void Update()
        {
            if (_loadingProgress == null)
            {
                return;
            }
            _progressBar.SetProgress(_loadingProgress.Value);
        }
    }
}
