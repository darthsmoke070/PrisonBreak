using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PrisonBreak.UI
{
    public class FillBar : MonoBehaviour
    {
        [SerializeField]
        private Image _progressImage;

        [SerializeField]
        private float _progressSpeed;

        private float _targetFillAmount;
        
        public void SetProgress(float fillAmount)
        {
            _targetFillAmount = fillAmount;
        }

        private void Update()
        {
            _progressImage.fillAmount = Mathf.Lerp(_progressImage.fillAmount, _targetFillAmount, Time.deltaTime * _progressSpeed);
        }
    }
}
