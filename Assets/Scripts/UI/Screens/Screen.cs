using System;
using UnityEngine;

namespace PrisonBreak.UI
{
    public abstract class Screen : MonoBehaviour
    {
        protected Tools.CustomCanvasScaler _canvasScaler;
        protected Camera _uiCamera;

        public void Init(Tools.CustomCanvasScaler canvasScaler, Camera uiCamera)
        {
            _canvasScaler = canvasScaler;
            _uiCamera = uiCamera;
        }

        public void Enable()
        {
            gameObject.SetActive(true);
        }

        // TODO probably just need to disable it?
        public void Disable()
        {
            Destroy(gameObject);
        }        
    }
}