using UnityEngine.UI;

namespace PrisonBreak.UI
{
    public class EmptyGraphics : Graphic
    {
        public override void SetMaterialDirty() {}
        public override void SetVerticesDirty() {}

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            vh.Clear();
            return;
        }
    }
}
