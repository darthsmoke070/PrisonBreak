using UnityEngine;
using UnityEngine.UI;

namespace PrisonBreak.UI
{
    public class MenuSwitcher : MonoBehaviour
    {
        [SerializeField]
        private Button _button;
        public Button Button => _button;

        [SerializeField]
        private GameObject _menuRoot;

        [SerializeField]
        private GameObject _gameplayPanelRoot;

        public void Init(bool isGameplayPanelActive)
        {
            _menuRoot.SetActive(!isGameplayPanelActive);
            _gameplayPanelRoot.SetActive(isGameplayPanelActive);
            _button.onClick.RemoveListener(Switch);
            _button.onClick.AddListener(Switch);
        }

        private void Switch()
        {
            _menuRoot.SetActive(!_menuRoot.activeSelf);
            _gameplayPanelRoot.SetActive(!_gameplayPanelRoot.activeSelf);
        }
    }
}
