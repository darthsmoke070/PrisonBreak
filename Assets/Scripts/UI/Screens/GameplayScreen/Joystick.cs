using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace PrisonBreak.UI
{
    public class Joystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        [SerializeField]
        private float _radius = 100f;

        [SerializeField]
        private float _deadRadius = 5f;

        [SerializeField]
        private RectTransform _base;

        [SerializeField]
        private RectTransform _stick;

        [SerializeField]
        private UnityEngine.UI.Graphic[] _visuals;

        [SerializeField]
        private Tools.CustomCanvasScaler _canvasScaler; 

        public UnityEvent<Vector2> OnHandleDrag = new UnityEvent<Vector2>();

        private bool _isPressed;
        public bool IsPressed
        {
            get
            {
                return _isPressed && _distance > _deadRadius;
            }
            private set
            {
                _isPressed = value;
            }
        }
        private float _distance;
        private int _pointerId;

        public void Init(Tools.CustomCanvasScaler canvasScaler)
        {
            _canvasScaler = canvasScaler;
            _pointerId = -1;
        }

        public void OnDrag(PointerEventData data)
        {
            if (_pointerId != data.pointerId)
            {
                return;
            }
            HandleDrag(data);
        }

        public void OnPointerDown(PointerEventData data)
        {
            if (_pointerId != -1)
            {
                return;
            }
            _pointerId = data.pointerId;
            _base.anchoredPosition = _canvasScaler.GetScreenToCanvasPos(data.position);
            HandleDrag(data);
            SetActiveVisuals(true);
            IsPressed = true;
        }

        public void OnPointerUp(PointerEventData data)
        {
            if (_pointerId != data.pointerId)
            {
                return;
            }

            _stick.anchoredPosition = _base.anchoredPosition;
            _distance = 0f;
            IsPressed = false;
            _pointerId = -1;
            SetActiveVisuals(false);
            OnHandleDrag.Invoke(Vector2.zero);
        }

        private void HandleDrag(PointerEventData data)
        {
            if (_pointerId != data.pointerId)
            {
                return;
            }

            var delta = _canvasScaler.GetScreenToCanvasPos(data.position) - _base.anchoredPosition;
            _stick.anchoredPosition = _base.anchoredPosition + delta;
            var offset = _stick.anchoredPosition - _base.anchoredPosition;
            _distance = offset.magnitude;
            var normalizedOffset = offset.normalized;
            _base.anchoredPosition = _stick.anchoredPosition - normalizedOffset * Mathf.Min(_radius * 2f, _distance);
            // TODO too slow
            OnHandleDrag.Invoke(Vector2.ClampMagnitude(normalizedOffset * (_distance / (_radius * 2f)), 1f));
        }

        public void SetActiveVisuals(bool enabled)
        {
            foreach (var visual in _visuals)
            {
                visual.enabled = enabled;
            }
        }
    }
}
