using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.UI
{
    public class EnemyIndicatorElement : Tools.PoolableMonoBehavior
    {
        [SerializeField]
        private HealthBar _healthBar;

        [SerializeField]
        private TMPro.TextMeshProUGUI _characterNameLabel;

        public void SetData(Gameplay.Character.CharacterInfo characterInfo)
        {
            _healthBar.UpdateHealth(characterInfo.HealthInfo);
            _characterNameLabel.text = characterInfo.Name;
        }
    }
}
