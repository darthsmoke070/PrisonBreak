using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine;

namespace PrisonBreak.UI
{
    public class GameplayScreen : Screen
    {

        public struct InitData
        {
            public Gameplay.WorldInfo WorldInfo;
            public UnityAction OnRestartButtonClick;
            public UnityAction<Vector2> OnJoystickChangedDirection { get; private set; }

            public InitData(Gameplay.WorldInfo worldInfo, UnityAction onRestartButtonClick, UnityAction<Vector2> onJoystickChangedDirection)
            {
                WorldInfo = worldInfo;
                OnRestartButtonClick = onRestartButtonClick;
                OnJoystickChangedDirection = onJoystickChangedDirection;
            }
        }

        [SerializeField]
        private Button _restartButton;

        [SerializeField]
        private HoldButton _shotButton;

        [SerializeField]
        private Button _reloadButton;

        [SerializeField]
        private Joystick _joystick;

        [SerializeField]
        private MenuSwitcher _menuSwitcher;

        [SerializeField]
        private HealthBar _healthBar;

        [SerializeField]
        private WeaponSlot[] _weaponSlots;

        [SerializeField]
        private WeaponSlot _currentWeaponSlot;

        [SerializeField]
        private EnemiesIndidcatorRoot _enemiesIndicatorRoot;

        private Gameplay.WorldInfo _worldInfo;

        public void Init(InitData initData)
        {
            _menuSwitcher.Init(true);
            _worldInfo = initData.WorldInfo;

            UpdateHealth();
            _worldInfo.PlayerCharacterInfo.HealthInfo.Events.OnDamage.RemoveListener(UpdateHealth);
            _worldInfo.PlayerCharacterInfo.HealthInfo.Events.OnDamage.AddListener(UpdateHealth);
            
            AddListenerToButton(initData.OnRestartButtonClick, _restartButton);
            AddListenerToButton(() => ShotRequest(initData.WorldInfo.PlayerCharacterInfo.InputBridge), _shotButton.Button);
            AddListenerToButton(() => ReloadRequest(initData.WorldInfo.PlayerCharacterInfo.InputBridge), _reloadButton);
            
            InitJoystick(initData.OnJoystickChangedDirection);

            _currentWeaponSlot.SetData(_worldInfo.PlayerCharacterInfo.CurrentWeaponInfo);
            for (var i = 0; i < _weaponSlots.Length; i++)
            {
                if (i >= _worldInfo.PlayerCharacterInfo.Weapons.Count)
                {
                    _weaponSlots[i].SetData(null);
                } else
                {
                    _weaponSlots[i].SetData(_worldInfo.PlayerCharacterInfo.Weapons[i]);
                    var cachedIndex = i;
                    AddListenerToButton(() => { ChangeWeaponRequest(_weaponSlots[cachedIndex].WeaponInfo, initData.WorldInfo.PlayerCharacterInfo.InputBridge); }, _weaponSlots[cachedIndex].Button);
                }
            }
            _enemiesIndicatorRoot.Init(_worldInfo, _canvasScaler, _uiCamera);
        }

        private void UpdateHealth()
        {
            _healthBar.UpdateHealth(_worldInfo.PlayerCharacterInfo.HealthInfo);
        }

        private void AddListenerToButton(UnityAction listiner, Button button)
        {
            button.onClick.RemoveListener(listiner);
            button.onClick.AddListener(listiner);
        }

        private void ShotRequest(Gameplay.Character.InputBridge inputBridge)
        {
            inputBridge.HandleInputEvent(new Gameplay.Character.EquipmentLogic.UseWeaponTrigger(!_shotButton.IsPressed));
        }

        private void ReloadRequest(Gameplay.Character.InputBridge inputBridge)
        {
            inputBridge.HandleInputEvent(new Gameplay.Character.EquipmentLogic.ReloadEvent());
        }

        private void ChangeWeaponRequest(Gameplay.Weapon.WeaponInfo weaponInfo, Gameplay.Character.InputBridge inputBridge)
        {
            _currentWeaponSlot.SetData(weaponInfo);
            inputBridge.HandleInputEvent(new Gameplay.Character.EquipmentLogic.ChangeCurrentWeapon(weaponInfo));
        }

        private void InitJoystick(UnityAction<Vector2> onJoystickChangedDirection)
        {
            _joystick.OnHandleDrag.RemoveListener(onJoystickChangedDirection);
            _joystick.OnHandleDrag.AddListener(onJoystickChangedDirection);
            _joystick.Init(_canvasScaler);
            _joystick.SetActiveVisuals(false);
        }
    }
}
