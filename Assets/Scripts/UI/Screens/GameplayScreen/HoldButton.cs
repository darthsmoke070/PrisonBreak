using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace PrisonBreak.UI
{
    [RequireComponent(typeof(Button))]
    public class HoldButton : MonoBehaviour, IUpdateSelectedHandler, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField]
        private Button _button;
        public Button Button => _button;

        private bool _isPressed;
        public bool IsPressed => _isPressed;

        public void OnPointerDown(PointerEventData eventData)
        {
            _isPressed = true;
        }
        
        public void OnUpdateSelected(BaseEventData eventData)
        {
            if (_isPressed)
            {
                _button.onClick.Invoke();
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            _isPressed = false;
        }
    }
}
