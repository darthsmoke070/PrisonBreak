using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.UI
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField]
        private FillBar _fillBar;

        [SerializeField]
        private TMPro.TextMeshProUGUI _healthLabel;

        public void UpdateHealth(Gameplay.HealthInfo healthInfo)
        {
            _fillBar.SetProgress(healthInfo.currentHealth / (float)healthInfo.MaxHealth);
            _healthLabel.text = $"{healthInfo.currentHealth} / {healthInfo.MaxHealth}";
        }
    }
}
