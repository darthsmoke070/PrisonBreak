using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.UI
{
    public class EnemiesIndidcatorRoot : MonoBehaviour
    {
        [SerializeField]
        private EnemyIndicatorElement _elementPrefab;

        [SerializeField]
        private Tools.CustomCanvasScaler _canvasScaler;
        
        [SerializeField]
        private Gameplay.WorldInfo _worldInfo;

        [SerializeField]
        private Camera _uiCamera;

        [SerializeField]
        private Vector2 _offsetStep;

        private Dictionary<Gameplay.Character.CharacterInfo, EnemyIndicatorElement> _activeElementsMap = new Dictionary<Gameplay.Character.CharacterInfo, EnemyIndicatorElement>();

        public void Init(Gameplay.WorldInfo worldInfo, Tools.CustomCanvasScaler canvasScaler, Camera uiCamera)
        {
            _worldInfo = worldInfo;
            _canvasScaler = canvasScaler;
            _uiCamera = uiCamera;
        }

        private void Update()
        {
            foreach (var enemy in _worldInfo.Enemies)
            {
                UpdateCharacterInfo(enemy);
            }
        }

        // TODO heavy
        private void UpdateCharacterInfo(Gameplay.Character.CharacterInfo characterInfo)
        {
            if (IsCharacterVisible(characterInfo, out var screenPos))
            {
                EnemyIndicatorElement element;
                if (_activeElementsMap.ContainsKey(characterInfo))
                {
                    element = _activeElementsMap[characterInfo];
                } else
                {
                    element = Tools.PooledObjectsSpawner.Instance.Spawn(_elementPrefab, Vector2.zero, Quaternion.identity, transform);
                    element.SetData(characterInfo);
                    _activeElementsMap[characterInfo] = element;
                }
                element.gameObject.SetActive(true);
                RectTransformUtility.ScreenPointToLocalPointInRectangle(transform as RectTransform, screenPos, _uiCamera, out var localPoint);
                var rectTransform = element.transform as RectTransform;
                rectTransform.anchoredPosition = localPoint + _offsetStep * screenPos.z;
            } else
            {
                if (_activeElementsMap.ContainsKey(_worldInfo.PlayerCharacterInfo))
                {
                    _activeElementsMap[_worldInfo.PlayerCharacterInfo].gameObject.SetActive(false);
                    _activeElementsMap.Remove(_worldInfo.PlayerCharacterInfo);
                }
            }
        }

        private bool IsCharacterVisible(Gameplay.Character.CharacterInfo characterInfo, out Vector3 screenPos)
        {
            screenPos = _worldInfo.Camera.WorldToScreenPoint(characterInfo.Position);
            return screenPos.x > 0f && 
                   screenPos.x < UnityEngine.Screen.width && 
                   screenPos.y > 0f && 
                   screenPos.y < UnityEngine.Screen.height;
        }
    }
}
