using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

namespace PrisonBreak.UI
{
    public class WeaponSlot : MonoBehaviour
    {
        [SerializeField]
        private Button _button;
        public Button Button => _button;

        [SerializeField]
        private GameObject _notEmptySlotGO;

        [SerializeField]
        private GameObject _emptySlotGO;

        [SerializeField]
        private TextMeshProUGUI _weaponSlotName;

        [SerializeField]
        private TextMeshProUGUI _textMeshProAmmoLabel;

        [SerializeField]
        private Gameplay.Weapon.WeaponInfo _weaponInfo;
        public Gameplay.Weapon.WeaponInfo WeaponInfo => _weaponInfo;

        public void SetData(Gameplay.Weapon.WeaponInfo weaponInfo)
        {
            _weaponInfo = weaponInfo;
            _emptySlotGO.SetActive(weaponInfo == null);
            _notEmptySlotGO.SetActive(weaponInfo != null);
        }

        private void Update()
        {
            if (_weaponInfo == null)
            {
                return;
            }
            _weaponSlotName.text = _weaponInfo.WeaponName;
            _textMeshProAmmoLabel.text = $"{_weaponInfo.CurrentAmmoInShell} / {_weaponInfo.MaxAmmoInShell}\n({_weaponInfo.CurrentAmmoLeft} / {_weaponInfo.MaxAmmoLeft})";
        }
    }
}
