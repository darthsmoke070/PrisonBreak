using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Events;

namespace PrisonBreak.UI
{
    public class MainMenuScreen : Screen
    {
        [SerializeField]
        private Button _startButton;

        [SerializeField]
        private Button _exitFromGameButton;

        public void Init(UnityAction onStartButtonClick, UnityAction onExitButtonClick)
        {
            _startButton.onClick.RemoveListener(onStartButtonClick);
            _startButton.onClick.AddListener(onStartButtonClick);
            _exitFromGameButton.onClick.RemoveListener(onExitButtonClick);
            _exitFromGameButton.onClick.AddListener(onExitButtonClick);
        }
    }
}
