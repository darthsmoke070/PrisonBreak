using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace PrisonBreak.UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField]
        private RectTransform _screenRoot;

        [SerializeField]
        private MainMenuScreen _mainMenuScreenPrefab;

        [SerializeField]
        private GameplayScreen _gameplayScreenPrefab;

        [SerializeField]
        private LoadScreen _loadScreenPrefab;

        [SerializeField]
        private Tools.CustomCanvasScaler _canvasScaler;

        [SerializeField]
        private Camera _uiCamera;

        private Screen _currentScreen;

        public void ActivateLoadScreen(Tools.FloatReference loadProgress)
        {
            var screen = ChangeScreen(_loadScreenPrefab);
            screen.Init(loadProgress);
        }

        public void ActivateGameScreen(GameplayScreen.InitData initData)
        {
            var screen = ChangeScreen(_gameplayScreenPrefab);
            screen.Init(initData);
        }

        public void ActivateMainMenu(UnityAction onStartButtonClick, UnityAction onExitButtonClick)
        {
            var screen = ChangeScreen(_mainMenuScreenPrefab);
            screen.Init(onStartButtonClick, onExitButtonClick);
        }

        private T ChangeScreen<T>(T screenPrefab) where T : Screen
        {
            if (_currentScreen != null)
            {
                _currentScreen.Disable();
            }
            _currentScreen = Instantiate(screenPrefab, _screenRoot);
            _currentScreen.Init(_canvasScaler, _uiCamera);
            return (T)_currentScreen;
        }
    }
}
