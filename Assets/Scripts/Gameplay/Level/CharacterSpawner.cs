using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.Gameplay.Character
{
    public class CharacterSpawner : MonoBehaviour
    {
        [SerializeField]
        private CharacterInfo _characterInfo;

        public void SetData(CharacterInfo characterInfo)
        {
            _characterInfo = characterInfo;
        }

        public Character Spawn(InputBridge inputBridge, bool reloadWeapon)
        {
            if (inputBridge == null)
            {
                Debug.LogError("Can't create character without input bridge");
                return null;
            }

            var characterInstance = Tools.PooledObjectsSpawner.Instance.Spawn(_characterInfo.Prefab, transform.position, transform.rotation, null);
            var characterInfo = Instantiate(_characterInfo);
            characterInfo.InputBridge = inputBridge;
            characterInfo.HealthInfo = Instantiate(_characterInfo.HealthInfo);
            characterInfo.Weapons.Clear();
            foreach (var weapon in _characterInfo.Weapons)
            {
                var newWeapon = Instantiate(weapon);
                newWeapon.CurrentAmmoLeft = weapon.CurrentAmmoLeft;
                newWeapon.CurrentAmmoInShell = weapon.CurrentAmmoInShell;
                characterInfo.Weapons.Add(newWeapon);
            }
            if (reloadWeapon)
            {
                foreach (var weapon in characterInfo.Weapons)
                {
                    weapon.CurrentAmmoInShell = weapon.MaxAmmoInShell;
                    weapon.CurrentAmmoLeft = weapon.MaxAmmoLeft;
                }
            }
            characterInfo.CurrentWeaponInfo = characterInfo.Weapons[0];
            Debug.Log(characterInfo.CurrentWeaponInfo.CurrentAmmoInShell);
            characterInfo.SetReferTransform(characterInstance.transform);
            characterInstance.Init(characterInfo);
            return characterInstance;
        }
    }
}
