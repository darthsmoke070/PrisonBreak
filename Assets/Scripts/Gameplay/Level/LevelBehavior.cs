using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.Gameplay
{
    public class LevelBehavior : MonoBehaviour
    {
        public struct LevelInfo
        {
            public Character.Character playerCharacterInfo;
        }

        [SerializeField]
        private Character.CharacterSpawner _playerCharacterSpawner;

        [SerializeField]
        private List<SpawnSpot> _spots;

        [SerializeField]
        private NextLevelDoor _nextLevelDoor;
        public NextLevelDoor NextLevelDoor => _nextLevelDoor;

        [SerializeField]
        private Camera _camera;
        public Camera Camera => _camera;

        [SerializeField]
        private Cinemachine.CinemachineVirtualCamera _followedCamera;

        public LevelInfo Spawn(Character.InputBridge playerInputBridge, Character.CharacterInfo characterInfo)
        {
            var result = new LevelInfo();
            if (characterInfo != null)
            {
                _playerCharacterSpawner.SetData(characterInfo);
            }
            result.playerCharacterInfo = _playerCharacterSpawner.Spawn(playerInputBridge, characterInfo == null);
            if (_followedCamera != null)
            {
                _followedCamera.LookAt = result.playerCharacterInfo.transform;
                _followedCamera.Follow = Tools.TransformFollower.Generate(result.playerCharacterInfo.transform, result.playerCharacterInfo.transform.rotation).transform;
            }
            return result;
        }
    }
}
