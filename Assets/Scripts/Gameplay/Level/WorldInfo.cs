using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.Gameplay
{
    [CreateAssetMenu]
    public class WorldInfo : ScriptableObject
    {
        [SerializeField]
        private Character.CharacterInfo _playerCharacterInfo;
        public Character.CharacterInfo PlayerCharacterInfo => _playerCharacterInfo;

        [SerializeField]
        private List<Character.CharacterInfo> _enemies = new List<Character.CharacterInfo>();
        public List<Character.CharacterInfo> Enemies => _enemies;

        [SerializeField]
        private Camera _camera;
        public Camera Camera => _camera;

        public static WorldInfo Generate(Character.Character playerCharacter, Camera camera)
        {
            var result = CreateInstance<WorldInfo>();
            result._playerCharacterInfo = playerCharacter.CharacterInfo;
            result._camera = camera;
            return result;
        }
    }
}
