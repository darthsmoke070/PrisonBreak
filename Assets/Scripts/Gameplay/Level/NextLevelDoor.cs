using UnityEngine;
using UnityEngine.Events;

namespace PrisonBreak.Gameplay
{
    public class NextLevelDoor : MonoBehaviour
    {

        [SerializeField]
        private LevelData _nextLevel;

        [SerializeField]
        private WorldInfo _worldInfo;

        public UnityEvent<LevelData> onPlayerPassThrow = new UnityEvent<LevelData>();

        public void Init(WorldInfo worldInfo)
        {
            _worldInfo = worldInfo;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent<Character.Character>(out var character) && character.CharacterInfo == _worldInfo.PlayerCharacterInfo)
            {
                onPlayerPassThrow.Invoke(_nextLevel);
            }
        }
    }
}
