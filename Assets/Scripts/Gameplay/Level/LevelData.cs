using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.Gameplay
{
    [CreateAssetMenu]
    public class LevelData : ScriptableObject
    {
        public string sceneName;
    }
}
