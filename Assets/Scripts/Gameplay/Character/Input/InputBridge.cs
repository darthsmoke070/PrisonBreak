using System;

namespace PrisonBreak.Gameplay.Character
{
    public class InputBridge
    {
        public Action<IInputEvent> inputEventListiner = delegate { };

        public void HandleInputEvent(IInputEvent inputEvent)
        {
            inputEventListiner.Invoke(inputEvent);
        }
    }
}
