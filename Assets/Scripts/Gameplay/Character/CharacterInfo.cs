using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.Gameplay.Character
{
    [CreateAssetMenu]
    public class CharacterInfo : ScriptableObject
    {
        [SerializeField]
        private string _name;
        public string Name => _name;
        
        [SerializeField]
        private Character _prefab;
        public Character Prefab => _prefab;

        [SerializeField]
        private List<Weapon.WeaponInfo> _weapons;
        public List<Weapon.WeaponInfo> Weapons => _weapons;

        [SerializeField]
        private HealthInfo _healthInfo;
        public HealthInfo HealthInfo 
        { 
            get
            {
                return _healthInfo;
            }
            set
            {
                _healthInfo = value;
            }
        }

        public Weapon.WeaponInfo CurrentWeaponInfo { get; set; }
        public InputBridge InputBridge { get; set; }
        private Transform _transform;
        public Vector3 Position { get { return _transform.position; } }

        public void SetReferTransform(Transform transform)
        {
            _transform = transform;
        }
    }
}
