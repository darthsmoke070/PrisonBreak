using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.Gameplay.Character
{
    public class Character : Tools.PoolableMonoBehavior, Weapon.TargetedMissileWeapon.IAimable
    {
        [SerializeField]
        private CharacterInfo _characterInfo;
        public CharacterInfo CharacterInfo => _characterInfo;

        [SerializeField]
        private Health _health;

        [SerializeField]
        private List<AbstractLogic> _logics = new List<AbstractLogic>();

        public void Init(CharacterInfo characterInfo)
        {
            _characterInfo = characterInfo;
            foreach (var logic in _logics)
            {
                logic.Init(_characterInfo);
            }
            _health.CurrentHealthInfo.Events.OnDeath.RemoveListener(OnCharacterDeath);
            _health.CurrentHealthInfo.Events.OnDeath.AddListener(OnCharacterDeath);
        }

        private void OnCharacterDeath()
        {
            if (_health.CurrentHealthInfo.IsDead())
            {
                StopAllCoroutines();
                StartCoroutine(WaitUntilCanBeDisabled());
            }
        }

        private IEnumerator WaitUntilCanBeDisabled()
        {
            var canBeDisabled = true;
            do
            {
                for (var i = 0; i < _logics.Count; i++)
                {
                    canBeDisabled &= _logics[i].CanCharacterBeDisabled();
                }
                yield return null;
            } while (!canBeDisabled);
            Disable(false);
        }

        private void Update()
        {
            foreach (var logic in _logics)
            {
                logic.OnUpdate();
            }

        }

        private void FixedUpdate()
        {
            foreach (var logic in _logics)
            {
                logic.OnFixedUpdate();
            }
        }

        public AbstractLogic TryToGetAbstractLogic<T>() where T : AbstractLogic
        {
            foreach (var logic in _logics)
            {
                if (logic is T)
                {
                    return logic;
                }
            }
            return null;
        }
    }
}
