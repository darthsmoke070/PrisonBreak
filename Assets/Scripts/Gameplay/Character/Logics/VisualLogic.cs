using UnityEngine;

namespace PrisonBreak.Gameplay.Character
{
    public class VisualLogic : AbstractLogic
    {
        [SerializeField]
        private Transform _visualsRoot;

        [SerializeField]
        private float _rotationSpeed;

        [SerializeField]
        private Material _characterMaterial;

        public override void Init(CharacterInfo characterInfo)
        {
            base.Init(characterInfo);
            characterInfo.HealthInfo.Events.OnDeath.RemoveListener(OnDeath);
            characterInfo.HealthInfo.Events.OnDeath.AddListener(OnDeath);
            characterInfo.HealthInfo.Events.OnRevive.RemoveListener(OnRevive);
            characterInfo.HealthInfo.Events.OnRevive.AddListener(OnRevive);
        }

        private void OnDeath()
        {
            // call Disolve Coroutine here
            _visualsRoot.gameObject.SetActive(false);
        }

        private void OnRevive()
        {
            _visualsRoot.gameObject.SetActive(true);
        }

        // is character visible?
        public override bool CanCharacterBeDisabled()
        {
            return !_visualsRoot.gameObject.activeSelf;
        }
    }
}
