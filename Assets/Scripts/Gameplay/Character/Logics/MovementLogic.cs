using UnityEngine;

namespace PrisonBreak.Gameplay.Character
{
    public class MovementLogic : AbstractLogic
    {
        public struct ChangeMoveDirection : IInputEvent
        {
            public Vector3 dir { get; private set; }

            public ChangeMoveDirection(Vector3 dir)
            {
                this.dir = dir;
            }
        }

        [SerializeField]
        private CharacterController _characterController;

        [SerializeField]
        private float _speed;

        [SerializeField]
        private float _rotationSpeed;

        private Vector3 _inputVelocity;
        private float _rotation;

        public override void Init(CharacterInfo characterInfo)
        {
            base.Init(characterInfo);
            characterInfo.InputBridge.inputEventListiner -= OnInputEventRecieved;
            characterInfo.InputBridge.inputEventListiner += OnInputEventRecieved;
            _rotation = transform.rotation.eulerAngles.y;
        }

        private void OnInputEventRecieved(IInputEvent inputEvent)
        {
            if (inputEvent is ChangeMoveDirection moveDirEvent)
            {
                _inputVelocity = moveDirEvent.dir;
            }
        }

        public override void OnUpdate()
        {
            base.OnUpdate();

            _characterController.Move(_inputVelocity * _speed * Time.deltaTime);

            if (_inputVelocity.sqrMagnitude > 0f)
            {
                _rotation = Mathf.Atan2(_inputVelocity.x, _inputVelocity.z) * Mathf.Rad2Deg;
            }
            _characterController.transform.rotation = Quaternion.Lerp(_characterController.transform.rotation, Quaternion.Euler(0f, _rotation, 0f), Time.deltaTime * _rotationSpeed);
        }

        public override bool CanCharacterBeDisabled()
        {
            return true;
        }
    }
}
