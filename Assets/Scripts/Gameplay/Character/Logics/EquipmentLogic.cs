using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.Gameplay.Character
{
    public class EquipmentLogic : AbstractLogic
    {
        public struct ReloadEvent : IInputEvent { }
        public struct UseWeaponTrigger : IInputEvent 
        {
            public bool releaseTrigger;
            public UseWeaponTrigger(bool releaseTrigger)
            {
                this.releaseTrigger = releaseTrigger;
            }
        }
        public struct GetSelectedLoot : IInputEvent { }
        public struct ChangeCurrentWeapon : IInputEvent 
        { 
            public Weapon.WeaponInfo weapon { get; private set; }
            public ChangeCurrentWeapon(Weapon.WeaponInfo weapon)
            {
                this.weapon = weapon;
            }
        }

        private Dictionary<Weapon.WeaponInfo, Weapon.AbstractWeapon> _weapons = new Dictionary<Weapon.WeaponInfo, Weapon.AbstractWeapon>();

        [SerializeField]
        private Transform _weaponRoot;

        private CharacterInfo _characterInfo;

        public override void Init(CharacterInfo characterInfo)
        {
            base.Init(characterInfo);
            _characterInfo = characterInfo;


            characterInfo.InputBridge.inputEventListiner -= OnInputEventRecieved;
            characterInfo.InputBridge.inputEventListiner += OnInputEventRecieved;

            foreach (var weapon in characterInfo.Weapons)
            {
                var weaponInstance = Tools.PooledObjectsSpawner.Instance.Spawn(weapon.Prefab, _weaponRoot.position, _weaponRoot.rotation, _weaponRoot);
                weaponInstance.Init(weapon);
                _weapons[weapon] = weaponInstance;
            }
            SelectWeapon(characterInfo.CurrentWeaponInfo);

            characterInfo.HealthInfo.Events.OnDeath.RemoveListener(OnCharacterDead);
            characterInfo.HealthInfo.Events.OnDeath.AddListener(OnCharacterDead);
        }

        private void OnInputEventRecieved(IInputEvent inputEvent)
        {
            switch (inputEvent)
            {
                case ReloadEvent reloadEvent:
                    ReloadCurrentWeapon();
                    break;
                case UseWeaponTrigger makeShot:
                    UseTriggerFromCurrentWeapon(makeShot.releaseTrigger);
                    break;
                case GetSelectedLoot getSelectedLoot:
                    //TryToGetSelectedLoot();
                    break;
                case ChangeCurrentWeapon changeCurrentWeapon:
                    SelectWeapon(changeCurrentWeapon.weapon);
                    break;
            }
        }

        private void ReloadCurrentWeapon()
        {
            if (_weapons.Count == 0)
            {
                return;
            }
            _weapons[_characterInfo.CurrentWeaponInfo].Reload();
        }

        private void UseTriggerFromCurrentWeapon(bool isReleased)
        {
            if (_weapons.Count == 0)
            {
                return;
            }
            if (isReleased)
            {
                _weapons[_characterInfo.CurrentWeaponInfo].ReleaseTrigger();
            } else
            {
                _weapons[_characterInfo.CurrentWeaponInfo].PullTrigger();
            }
        }

        public void SelectWeapon(Weapon.WeaponInfo weaponInfo)
        {
            DisableAllWeapons();
            if (!_weapons.ContainsKey(weaponInfo))
            {
                return;
            }
            _characterInfo.CurrentWeaponInfo = weaponInfo;
            _weapons[_characterInfo.CurrentWeaponInfo].SetActive(true);
        }

        private void OnCharacterDead()
        {
            DisableAllWeapons();
        }

        private void DisableAllWeapons()
        {
            foreach (var weapon in _weapons)
            {
                weapon.Value.SetActive(false);
            }
        }

        public override bool CanCharacterBeDisabled()
        {
            return true;
        }
    }
}