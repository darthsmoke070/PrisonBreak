using UnityEngine;

namespace PrisonBreak.Gameplay.Character
{
    public abstract class AbstractLogic : MonoBehaviour
    {
        public virtual void Init(CharacterInfo characterInfo)
        {

        }

        public virtual void OnUpdate()
        {

        }

        public virtual void OnFixedUpdate()
        {

        }

        public abstract bool CanCharacterBeDisabled();
    }
}
