using UnityEngine;

namespace PrisonBreak.Gameplay.Weapon
{
    public class MissileWeapon : AbstractWeapon
    {
        [SerializeField]
        private Missile _missilePrefab;

        protected override void MakeSingleShot()
        {
            base.MakeSingleShot();
            var missile = Tools.PooledObjectsSpawner.Instance.Spawn(_missilePrefab, transform.TransformPoint(_muzzlePosition), transform.rotation, null);
            missile.Enable(true);
            ThrowMissile(missile);
        }

        protected virtual void ThrowMissile(Missile missile)
        {
            missile.Throw(null);
        }
    }
}