using UnityEngine;
using UnityEngine.Events;

namespace PrisonBreak.Gameplay.Weapon
{
    public class Missile : Tools.PoolableMonoBehavior
    {
        [SerializeField]
        private Rigidbody _rigidbody;

        [SerializeField]
        private float _missileSpeed = 5f;
        
        [SerializeField]
        private int _damage = 2;

        [SerializeField]
        private float _lifeTime = 10;

        [SerializeField]
        private UnityEvent _onHit = new UnityEvent();

        [SerializeField]
        private UnityEvent _onExplode = new UnityEvent();

        private Transform _target;
        private float _lifeTimer;

        public void Throw(Transform target)
        {
            _target = target;
        }

        public override void Enable(bool immediately)
        {
            base.Enable(immediately);
            _lifeTimer = 0f;
        }

        private void Update()
        {
            if (!IsEnabled)
            {
                return;
            }

            // TO DO using _target.gameObject.activeSelf is kinda dumb, need to find a better way to do it
            if (_target != null && _target.gameObject.activeSelf)
            {
                transform.LookAt(_target);
            }

            var dt = Time.deltaTime;

            var nextPosition = _rigidbody.position + transform.forward * _missileSpeed * dt;
            _rigidbody.position = nextPosition;

            if (_lifeTimer >= _lifeTime)
            {
                Explode(null);
            }
            _lifeTimer += dt;
        }

        private void OnTriggerEnter(Collider other)
        {
            _onHit.Invoke();
            Explode(other);
        }

        public void Explode(Collider collider)
        {
            if (collider != null && collider.gameObject.TryGetComponent<Health>(out var health))
            {
                health.DealDamage(_damage);
            }
            _onExplode.Invoke();
            Disable(false);
        }
    }
}