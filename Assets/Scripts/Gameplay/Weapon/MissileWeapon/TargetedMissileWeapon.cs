using UnityEngine;

namespace PrisonBreak.Gameplay.Weapon
{
    public class TargetedMissileWeapon : MissileWeapon
    {
        public interface IAimable { };

        [SerializeField]
        private Transform _aim;

        [SerializeField]
        private float _targetRadius;

        protected override void ThrowMissile(Missile missile)
        {
            var possibleTargets = Physics.OverlapSphere(_aim.position, _targetRadius);
            Transform followingTransform = null;
            foreach (var pt in possibleTargets)
            {
                if (pt.gameObject.TryGetComponent<IAimable>(out var targetable))
                {
                    followingTransform = (targetable as MonoBehaviour).transform;
                    break;
                }
            }
            missile.Throw(followingTransform);
        }

        private void OnDrawGizmos()
        {
            if (_aim == null)
            {
                return;
            }
            Gizmos.color = new Color(0f, 1f, 0f, 0.2f);
            Gizmos.DrawSphere(_aim.transform.position, _targetRadius);
        }
    }
}
