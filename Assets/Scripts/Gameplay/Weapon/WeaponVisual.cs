using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.Gameplay.Weapon
{
    public class WeaponVisual : MonoBehaviour
    {
        public virtual void Init(WeaponEvents events)
        {
            // subscribe required visual effects
        }
    }
}
