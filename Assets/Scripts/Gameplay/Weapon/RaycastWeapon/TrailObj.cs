using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.Gameplay.Weapon
{
    public class TrailObj : Tools.PoolableMonoBehavior
    {
        [SerializeField]
        private TrailRenderer _trail;
        public TrailRenderer Trail => _trail;
        
        [SerializeField]
        private float _speed;

        private Vector3 _startPos;
        private float _sqrDistance;
        private float _sqrRemainingDistance;
        private Vector3 _target;

        public void Init(Vector3 target)
        {
            _startPos = _trail.transform.position;
            _sqrDistance = (target - _trail.transform.position).sqrMagnitude;
            _sqrRemainingDistance = _sqrDistance;
            _target = target;
            Enable(true);
        }

        public override void ResetGO()
        {
            base.ResetGO();

        }

        private void Update()
        {
            if (_sqrRemainingDistance <= 0)
            {
                return;
            }
            _trail.transform.position = Vector3.Lerp(_startPos, _target, 1 - (_sqrRemainingDistance / _sqrDistance));
            _sqrRemainingDistance -= _speed * Time.deltaTime;
            if (_sqrRemainingDistance <= 0)
            {
                Disable(false);
            }
        }
    }
}
