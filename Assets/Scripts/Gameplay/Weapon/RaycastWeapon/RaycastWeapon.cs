using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.Gameplay.Weapon
{
    public class RaycastWeapon : AbstractWeapon
    {

        [SerializeField]
        private List<Vector3> _directions;

        [SerializeField]
        private LayerMask _layers;

        [SerializeField]
        private int _damage;

        protected override void MakeSingleShot()
        {
            if (TryToReloadAmmo())
            {
                return;
            }

            for (var i = 0; i < _directions.Count; i++)
            {
                var direction = transform.TransformPoint(_directions[i]);
                var fromPos = transform.TransformPoint(_muzzlePosition);
                if (Physics.Raycast(fromPos, direction, out var hitInfo))
                {
                    if (hitInfo.collider.TryGetComponent<Health>(out var health))
                    {
                        health.DealDamage(_damage);
                    }
                    _weaponEvents.onMakeShot.Invoke(hitInfo.point);
                } else
                {
                    _weaponEvents.onMakeShot.Invoke(direction);
                }
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            for (var i = 0; i < _directions.Count; i++)
            {
                Gizmos.DrawRay(transform.TransformPoint(_muzzlePosition), transform.TransformDirection(_directions[i]));
            }
        }
    }

}
