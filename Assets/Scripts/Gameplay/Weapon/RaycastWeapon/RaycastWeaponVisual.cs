using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.Gameplay.Weapon
{
    public class RaycastWeaponVisual : WeaponVisual
    {
        [SerializeField]
        private TrailObj _trailPrefab;

        [SerializeField]
        private Vector3 _muzzleFlashPos;

        public override void Init(WeaponEvents events)
        {
            base.Init(events);
            events.onMakeShot.RemoveListener(OnShot);
            events.onMakeShot.AddListener(OnShot);
        }

        private void OnShot(Vector3 target)
        {
            var trail = Tools.PooledObjectsSpawner.Instance.Spawn(_trailPrefab, transform.TransformPoint(_muzzleFlashPos), transform.rotation, null);
            trail.Init(target);
        }
    }
}
