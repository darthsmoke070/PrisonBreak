using System.Collections;
using UnityEngine;

namespace PrisonBreak.Gameplay.Weapon
{
    public abstract class AbstractWeapon : Tools.PoolableMonoBehavior
    {
        [SerializeField]
        private WeaponInfo _weaponInfo;

        [SerializeField]
        protected Vector3 _muzzlePosition;

        [SerializeField]
        protected WeaponEvents _weaponEvents = new WeaponEvents();
        public WeaponEvents WeaponEvents => _weaponEvents;

        [SerializeField]
        protected WeaponVisual _weaponVisual;

        public override bool IsEnabled => _weaponVisual.gameObject.activeSelf;

        private float _automaticBurstTimer;
        private bool _isShotInProcessing;
        private bool _isTriggerd;
        private bool _prevIsTriggerd;

        public virtual void Init(WeaponInfo weaponInfo)
        {
            _weaponVisual.Init(_weaponEvents);
            _weaponInfo = weaponInfo;
        }

        public virtual bool CanWeaponBeReloaded()
        {
            return _weaponInfo.CurrentAmmoLeft == 0 || 
                    !_isShotInProcessing;
        }

        public virtual void PullTrigger()
        {
            _isTriggerd = true;
            _weaponEvents.onPullTrigger.Invoke();
        }

        protected virtual void Update()
        {
            if (_weaponInfo.IsAutomatic)
            {
                if (_isTriggerd && !_isShotInProcessing && _automaticBurstTimer > _weaponInfo.SecondsForOneBurst + _weaponInfo.SecondsForOneShot * _weaponInfo.ShotsPerTriggerCount)
                {
                    StopCoroutineAndStartShooting();
                } else
                {
                    _automaticBurstTimer += Time.deltaTime;
                }
            }

            if (_isTriggerd && !_prevIsTriggerd && !_isShotInProcessing) 
            {
                StopCoroutineAndStartShooting();
            }

            _prevIsTriggerd = _isTriggerd;
        }

        private void StopCoroutineAndStartShooting()
        {
            StopAllCoroutines();
            if (_weaponInfo.CurrentAmmoLeft == 0 && _weaponInfo.CurrentAmmoInShell == 0)
            {
                return;
            }
            StartCoroutine(MakeShots());
        }

        public void StopShootingProcess()
        {
            StopAllCoroutines();
            _automaticBurstTimer = 0f;
            _isShotInProcessing = false;
        }

        protected virtual IEnumerator MakeShots() {
            _automaticBurstTimer = 0f;
            _isShotInProcessing = true;
            var shotsCounter = 0;
            do
            {
                MakeSingleShot();
                yield return new WaitForSeconds(_weaponInfo.SecondsForOneShot);
                shotsCounter++;
            } while (shotsCounter < _weaponInfo.ShotsPerTriggerCount);
            _isShotInProcessing = false;
        }

        protected virtual void MakeSingleShot()
        {
            _weaponInfo.CurrentAmmoInShell = Mathf.Max(_weaponInfo.CurrentAmmoInShell - 1, 0);
            if (TryToReloadAmmo())
            {
                return;
            }
            _weaponEvents.onMakeShot.Invoke(Vector3.zero);
        }

        protected bool TryToReloadAmmo()
        {
            if (_weaponInfo.CurrentAmmoInShell == 0)
            {
                Reload();
                return true;
            }
            return false;
        }

        public void ReleaseTrigger()
        {
            _isTriggerd = false;
            _weaponEvents.onReleaseTrigger.Invoke();
        }

        public virtual void Reload()
        {
            StopShootingProcess();
            if (!CanWeaponBeReloaded())
            {
                _weaponEvents.onReloadFailed.Invoke();
                return;
            }

            var requiredAmmo = _weaponInfo.MaxAmmoInShell - _weaponInfo.CurrentAmmoInShell;
            if (_weaponInfo.CurrentAmmoLeft < requiredAmmo)
            {
                requiredAmmo = _weaponInfo.CurrentAmmoLeft;
            }
            _weaponInfo.CurrentAmmoLeft -= requiredAmmo;
            _weaponInfo.CurrentAmmoInShell += requiredAmmo;

            _weaponEvents.onReloadSuccess.Invoke();
        }

        public void SetActive(bool enabled)
        {
            _weaponVisual.gameObject.SetActive(enabled);
        }

    }
}
