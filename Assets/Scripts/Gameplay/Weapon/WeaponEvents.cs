using UnityEngine.Events;
using UnityEngine;

namespace PrisonBreak.Gameplay.Weapon
{
    [System.Serializable]
    public class WeaponEvents
    {
        public UnityEvent onPullTrigger = new UnityEvent();
        public UnityEvent onReloadSuccess = new UnityEvent();
        public UnityEvent onReloadFailed = new UnityEvent();
        public UnityEvent<Vector3> onMakeShot = new UnityEvent<Vector3>();
        public UnityEvent onReleaseTrigger = new UnityEvent();
    }
}
