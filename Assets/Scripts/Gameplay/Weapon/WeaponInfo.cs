using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.Gameplay.Weapon
{
    [CreateAssetMenu]
    public class WeaponInfo : ScriptableObject
    {
        [SerializeField]
        private string _weaponName;
        public string WeaponName => _weaponName;

        [SerializeField]
        private AbstractWeapon _prefab;
        public AbstractWeapon Prefab => _prefab;

        [SerializeField]
        private bool _isAutomatic;
        public bool IsAutomatic => _isAutomatic;

        [SerializeField]
        private int _shotsPerTriggerCount;
        public int ShotsPerTriggerCount => _shotsPerTriggerCount;

        [SerializeField]
        private float _secondsForOneShot;
        public int SecondsForOneShot => _shotsPerTriggerCount;

        [SerializeField]
        private float _secondsForOneBurst;
        public float SecondsForOneBurst => _secondsForOneBurst;

        [SerializeField]
        private int _maxAmmoLeft = 90;
        public int MaxAmmoLeft => _maxAmmoLeft;

        [SerializeField]
        private int _maxAmmoInShell = 30;
        public int MaxAmmoInShell => _maxAmmoInShell;

        public int CurrentAmmoLeft { get; set; }
        public int CurrentAmmoInShell { get; set; }
    }
}
