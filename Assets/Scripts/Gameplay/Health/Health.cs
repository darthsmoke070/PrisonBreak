using UnityEngine;

namespace PrisonBreak.Gameplay
{
    public class Health : MonoBehaviour
    {
        [SerializeField]
        private HealthInfo.HealthEvents _healthEvents;

        [SerializeField]
        private HealthInfo _healthInfo;
        private HealthInfo _realtimeHealthInfo;
        public HealthInfo CurrentHealthInfo { 
            get 
            { 
                if (_realtimeHealthInfo == null)
                {
                    return _healthInfo;
                }
                return _realtimeHealthInfo; 
            } 
        }

        private void Awake()
        {
            _realtimeHealthInfo = Instantiate(_healthInfo);
            _realtimeHealthInfo.Events += _healthEvents;
        }

        public void DealDamage(int damage)
        {
            if (!enabled)
            {
                return;
            }

            CurrentHealthInfo.currentHealth = Mathf.Max(_healthInfo.currentHealth - damage, 0);
            CurrentHealthInfo.Events.OnDamage.Invoke();
            if (CurrentHealthInfo.IsDead())
            {
                CurrentHealthInfo.Events.OnDeath.Invoke();
                enabled = false;
            }
        }
        
        public void Revive()
        {
            CurrentHealthInfo.ResetInfo();
            CurrentHealthInfo.Events.OnRevive.Invoke();
            enabled = true;
        }
    }
}
