using UnityEngine;
using UnityEngine.Events;

namespace PrisonBreak.Gameplay
{
    [CreateAssetMenu]
    public class HealthInfo : ScriptableObject
    {
        [SerializeField]
        private int _maxHealth;
        public int MaxHealth => _maxHealth;
        public int currentHealth;

        [SerializeField]
        private HealthEvents _healthEvents;
        public HealthEvents Events { get { return _healthEvents; } set { _healthEvents = value; } }

        public bool IsDead()
        {
            return currentHealth == 0;
        }

        public void ResetInfo()
        {
            currentHealth = _maxHealth;
        }

        [System.Serializable]
        public struct HealthEvents
        {
            [SerializeField]
            private UnityEvent _onRevive;
            public UnityEvent OnRevive => _onRevive;

            [SerializeField]
            private UnityEvent _onDeath;
            public UnityEvent OnDeath => _onDeath;

            [SerializeField]
            private UnityEvent _onDamage;
            public UnityEvent OnDamage => _onDamage;
        
            public static HealthEvents operator +(HealthEvents a, HealthEvents b)
            {
                UnityAction eventListiner = () => b.OnDamage.Invoke();
                a.OnDamage.RemoveListener(eventListiner);
                a.OnDamage.AddListener(eventListiner);

                eventListiner = () => b.OnDeath.Invoke();
                a.OnDeath.RemoveListener(eventListiner);
                a.OnDeath.AddListener(eventListiner);

                eventListiner = () => b.OnRevive.Invoke();
                a.OnRevive.RemoveListener(eventListiner);
                a.OnRevive.AddListener(eventListiner);
                return a;
            }
        }
    }
}
