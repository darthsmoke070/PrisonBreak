using UnityEngine;

namespace PrisonBreak.Root
{
    public class ClientManager : MonoBehaviour
    {
        [SerializeField]
        private UI.UIManager _ui;

        [SerializeField]
        private Gameplay.LevelData _firstLevel;

        private ClientState _currentState;

        private void Start()
        {
            InitializeGame();
        }

        private void InitializeGame()
        {
            DontDestroyOnLoad(this);
            DontDestroyOnLoad(_ui);
            _currentState = MainMenuState.Generate(_ui, _firstLevel);
        }

        private void Update()
        {
            _currentState.UpdateState();
            var nextState = _currentState.GetNextState();
            if (_currentState != nextState)
            {
                Destroy(_currentState.gameObject);
                _currentState = nextState;
                DontDestroyOnLoad(_currentState);
            }
        }
    }
}
