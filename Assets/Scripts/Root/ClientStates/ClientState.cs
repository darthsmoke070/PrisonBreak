using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PrisonBreak.Root
{
    public abstract class ClientState : MonoBehaviour
    {
        public virtual void UpdateState() { }
        protected ClientState _nextState;
        public virtual ClientState GetNextState()
        {
            if (_nextState != null)
            {
                return _nextState;
            }
            return this;
        }
    }
}
