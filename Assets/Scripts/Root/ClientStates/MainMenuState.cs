using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace PrisonBreak.Root
{
    public class MainMenuState : ClientState
    {
        private UI.UIManager _ui;
        private Gameplay.LevelData _levelData;

        private void StartGameplay()
        {
            if (_nextState != null)
            {
                return;
            }
            _nextState = LoadLevelState.Generate(_ui, _levelData,  null);
        }

        private void ExitFromGame()
        {
#if UNITY_EDITOR
            EditorApplication.ExitPlaymode();
#endif
            Application.Quit();
        }

        public static MainMenuState Generate(UI.UIManager ui, Gameplay.LevelData firstLevel)
        {
            var go = new GameObject(nameof(MainMenuState));
            var result = go.AddComponent<MainMenuState>();
            ui.ActivateMainMenu(result.StartGameplay, result.ExitFromGame);
            result._ui = ui;
            result._levelData = firstLevel;
            return result;
        }
    }
}
