using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PrisonBreak.Root
{
    public class LoadLevelState : ClientState
    {

        private Tools.FloatReference _loadingProgress;
        private UI.UIManager _ui;

        public IEnumerator LoadLevelCoroutine(Gameplay.LevelData levelData, Gameplay.WorldInfo worldInfo)
        {
            var asyncLoad = SceneManager.LoadSceneAsync(levelData.sceneName);
            _loadingProgress.Value = asyncLoad.progress;
            while (!asyncLoad.isDone)
            {
                yield return null;
            }
            _nextState = GameplayState.Generate(_ui, levelData, worldInfo == null ? null : worldInfo.PlayerCharacterInfo, worldInfo);
        }

        public static LoadLevelState Generate(UI.UIManager ui, Gameplay.LevelData levelData, Gameplay.WorldInfo worldInfo)
        {
            var go = new GameObject(nameof(LoadLevelState));
            var result = go.AddComponent<LoadLevelState>();
            result._loadingProgress = ScriptableObject.CreateInstance<Tools.FloatReference>();
            result._loadingProgress.Value = 0f;
            result._ui = ui;
            ui.ActivateLoadScreen(result._loadingProgress);
            result.StartCoroutine(result.LoadLevelCoroutine(levelData, worldInfo));
            return result;
        }
    }
}