using UnityEngine;

namespace PrisonBreak.Root
{
    public class GameplayState : ClientState
    {
        private UI.UIManager _ui;
        private Gameplay.LevelBehavior _levelBehavior;
        private Gameplay.LevelData _currentLevelData;
        private Gameplay.WorldInfo _worldInfoOnStart;
        private Gameplay.WorldInfo _currentWorldInfo;
        private Camera _gameCamera;

        public override void UpdateState()
        {
            base.UpdateState();
            if (_nextState != null)
            {
                return;
            }
            // whatching on gameplay logic for find new state here
        }

        private void RestartLevel()
        {
            _nextState = LoadLevelState.Generate(_ui, _currentLevelData, _worldInfoOnStart);
        }

        private void LaunchLevel(Gameplay.LevelData levelData)
        {
            _nextState = LoadLevelState.Generate(_ui, levelData, _currentWorldInfo);
        }

        private void ChangePlayerDirectionRequest(Gameplay.Character.InputBridge inputBridge, Vector2 inputDirection)
        {
            // projection camera view on plane. Maybe better to use transorm.TransformPoint?
            var forward = _levelBehavior.Camera.transform.forward;
            var right = _levelBehavior.Camera.transform.right;
            forward.y = 0f;
            right.y = 0f;
            forward.Normalize();
            right.Normalize();
            var resultDirection = forward * inputDirection.y + right * inputDirection.x;
            inputBridge.HandleInputEvent(new Gameplay.Character.MovementLogic.ChangeMoveDirection(resultDirection));
        }

        public static GameplayState Generate(UI.UIManager ui, Gameplay.LevelData currentLevelData, Gameplay.Character.CharacterInfo playerInfo, Gameplay.WorldInfo worldInfoOnStart)
        {
            var go = new GameObject(nameof(GameplayState));
            var result = go.AddComponent<GameplayState>();
            result._levelBehavior = FindObjectOfType<Gameplay.LevelBehavior>();
            if (result._levelBehavior == null)
            {
                Debug.LogError("Can't find Level Behavior on scene");
                return null;
            }

            result._currentLevelData = currentLevelData;
            var playerCharacterInputBridge = new Gameplay.Character.InputBridge();
            Gameplay.LevelBehavior.LevelInfo levelInfo = result._levelBehavior.Spawn(playerCharacterInputBridge, playerInfo);
            if (worldInfoOnStart != null)
            {
                result._worldInfoOnStart = Instantiate(worldInfoOnStart);
            }
            result._currentWorldInfo = Gameplay.WorldInfo.Generate(levelInfo.playerCharacterInfo, result._levelBehavior.Camera);
            result._levelBehavior.NextLevelDoor.Init(result._currentWorldInfo);
            result._levelBehavior.NextLevelDoor.onPlayerPassThrow.RemoveListener(result.LaunchLevel);
            result._levelBehavior.NextLevelDoor.onPlayerPassThrow.AddListener(result.LaunchLevel);

            result._ui = ui;
            var gameplayScreenInitData = new UI.GameplayScreen.InitData(
                    result._currentWorldInfo,
                    result.RestartLevel,
                    (direction) => result.ChangePlayerDirectionRequest(playerCharacterInputBridge, direction)
                );
            result._ui.ActivateGameScreen(gameplayScreenInitData);
            return result;
        }
    }
}